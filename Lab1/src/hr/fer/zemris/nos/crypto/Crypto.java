package hr.fer.zemris.nos.crypto;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Crypto {

	public static Cipher aes(boolean encrypt, byte[] Key, byte[] IV) throws Exception {
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE");
		SecretKeySpec key = new SecretKeySpec(Key, "AES");
		if (IV == null)
			cipher.init(encrypt ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE, key);
		else
			cipher.init(encrypt ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE, key, new IvParameterSpec(IV));

		return cipher;
	}

	public static Cipher rsa(boolean encrypt, byte[] key, byte[] modulus) throws Exception {
		return rsa(encrypt, key, modulus, encrypt);// keytype == true->publickey
	}

	public static Cipher rsa(boolean encrypt, byte[] exp, byte[] modulus, boolean keyType) throws Exception {
		Cipher cipher = Cipher.getInstance("RSA/ECB/NoPadding", "SunJCE");

		KeyFactory kf = KeyFactory.getInstance("RSA");
		BigInteger mod = new BigInteger(modulus);
		BigInteger EXP = new BigInteger(exp);
		Key rsakey = keyType ? kf.generatePublic(new RSAPublicKeySpec(mod, EXP))
				: kf.generatePrivate(new RSAPrivateKeySpec(mod, EXP));

		cipher.init(encrypt ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE, rsakey);

		return cipher;
	}

	public static byte[] sha(String inputFile) throws Exception {
		MessageDigest md = MessageDigest.getInstance("SHA-1");
		BufferedInputStream bis = new BufferedInputStream(new FileInputStream(inputFile));

		int read = 0;
		byte[] inputBuff = new byte[4096];
		while ((read = bis.read(inputBuff)) >= 1) {
			md.update(inputBuff, 0, read);
		}
		bis.close();

		return md.digest();
	}

	public static byte[] generateSymmetricKey(String algorithm, int length) throws Exception {
		KeyGenerator keyGen = KeyGenerator.getInstance(algorithm, "SunJCE");
		keyGen.init(length);
		return keyGen.generateKey().getEncoded();
	}

	public static byte[] genetareIV() throws Exception {
		byte[] iv = new byte[16];
		SecureRandom prng = new SecureRandom();
		prng.nextBytes(iv);
		return iv;
	}

	public static byte[][] generateMineAsymmetricKey(int length) {
		byte[][] keys = new byte[3][];
		Random rnd = new Random();
		BigInteger p = BigInteger.probablePrime(length/2, rnd);
		BigInteger q = BigInteger.probablePrime(length/2, rnd);
		BigInteger n = p.multiply(q);
		BigInteger z = p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));
		BigInteger e = rndBigInt(z);
		while(e.gcd(z).compareTo(BigInteger.ONE)!=0)
			e = e.add(BigInteger.ONE);
		BigInteger d = e.modInverse(z);
		keys[0] = d.toByteArray();
		keys[1] = e.toByteArray();
		keys[2] = n.toByteArray();
		return keys;
	}

	
	public static BigInteger rndBigInt(BigInteger max) {
		Random rnd = new Random();
		do {
			BigInteger i = new BigInteger(max.bitLength(), rnd);
			if (i.compareTo(max) <= 0)
				return i;
		} while (true);
	}

	public static byte[][] generateAsymmetricKey(String algorithm, int length) throws Exception {
		byte[][] keys = new byte[3][];
		KeyPairGenerator keyGen = KeyPairGenerator.getInstance(algorithm);
		keyGen.initialize(length);
		KeyPair keyPair = keyGen.generateKeyPair();
		BigInteger prExp = ((RSAPrivateKey) keyPair.getPrivate()).getPrivateExponent();
		BigInteger pubExp = ((RSAPublicKey) keyPair.getPublic()).getPublicExponent();
		BigInteger mod = ((RSAPrivateKey) keyPair.getPrivate()).getModulus();
		keys[0] = prExp.toByteArray();
		keys[1] = pubExp.toByteArray();
		keys[2] = mod.toByteArray();
		return keys;
	}

}
