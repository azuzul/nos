package hr.fer.zemris.nos.crypto;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.xml.bind.DatatypeConverter;

public class MyCipher implements ICipher {

	private Cipher cipher;
	private boolean mine;
	private byte[] key;
	private byte[] modulus;
	private boolean encrypt;

	public MyCipher() {
	}

	public int getModulusSize(){
		return modulus.length-1;
	}
	
	public static ICipher generateRsa(boolean encrypt, byte[] key, byte[] modulus, boolean mine) throws Exception {
		return generateRsa(encrypt, key, encrypt, modulus, mine);
	}

	public static ICipher generateRsa(boolean encrypt, byte[] key, boolean keyType, byte[] modulus, boolean mine)
			throws Exception {
		MyCipher my = new MyCipher();
		my.mine = mine;
		my.encrypt = encrypt;
		my.modulus = modulus;
		if (mine) {
			my.key = key;
		} else {
			my.cipher = Crypto.rsa(encrypt, key, modulus, keyType);
		}
		return my;
	}

	@Override
	public byte[] doFinal(int len, byte[] data) throws Exception {
		byte[] output = null;
		if (mine) {
			BigInteger dat = new BigInteger(1, encrypt ? createpadding(modulus.length, data) : data);
			output = dat.modPow(new BigInteger(key), new BigInteger(modulus)).toByteArray();
		} else
			output = cipher.doFinal(data);
		if (encrypt) {
			if (mine && output[0] == 0 && output.length>getModulusSize())
				return Arrays.copyOfRange(output, 1, output.length);
			return output;
		}
		return removePadding(len / 8, output);
	}

	public static void main(String[] args) throws Exception {
		// Cryptodata cd = Cryptodata.parseDocument("rsa_a_public_key.txt",
		// false);

		 byte[][] keys = Crypto.generateAsymmetricKey("RSA", 1024);
//		byte[][] keys = Crypto.generateMineAsymmetricKey(1024);

		byte[] pubexp = keys[1];// cd.getPubExp()
		byte[] mod = keys[2];
		byte[] privExp = keys[0];

		ICipher cipher = MyCipher.generateRsa(true, pubexp, mod, true);
		ICipher cipher1 = MyCipher.generateRsa(true, pubexp, mod, false);

		byte[] AESkey =  DatatypeConverter.parseHexBinary("67EA473D3A4CEDE426E57535567B2BE1");//Crypto.generateSymmetricKey("AES", 128);//
		byte[] AESiv = Crypto.genetareIV();// DatatypeConverter.parseHexBinary("604184E861C07ADD6F7121F2440DED2C");
		
		
		System.out.println("Key: " + DatatypeConverter.printHexBinary(AESkey));
		System.out.println("IV: " + DatatypeConverter.printHexBinary(AESiv));

		byte[] cryptAesKey = cipher.doFinal(128, AESkey);
		byte[] cryptAESiv = cipher.doFinal(128, AESiv);

		byte[] cryptAesKey1 = cipher1.doFinal(128, AESkey);
		byte[] cryptAESiv1 = cipher1.doFinal(128, AESiv);

		System.out.println();
		System.out.println("Crypt Key: " + DatatypeConverter.printHexBinary(cryptAesKey));
		System.out.println("Crypt IV: " + DatatypeConverter.printHexBinary(cryptAESiv));

		System.out.println();
		System.out.println("Crypt Key: " + DatatypeConverter.printHexBinary(cryptAesKey1));
		System.out.println("Crypt IV: " + DatatypeConverter.printHexBinary(cryptAESiv1));

		System.out.println();
		System.out.println("key are same: " + (Arrays.equals(cryptAesKey, cryptAesKey1)));
		System.out.println("iv are same: " + (Arrays.equals(cryptAESiv, cryptAESiv1)));

		// cd = Cryptodata.parseDocument("rsa_a_private_key.txt", false);

		cipher = MyCipher.generateRsa(false, privExp, mod, true);
		cipher1 = MyCipher.generateRsa(false, privExp, mod, false);

		System.out.println();
		System.out.println("Crypt Key: " + DatatypeConverter.printHexBinary(cipher.doFinal(128, cryptAesKey1)));
		System.out.println("Crypt IV: " + DatatypeConverter.printHexBinary(cipher.doFinal(128, cryptAESiv1)));

		System.out.println();
		System.out.println("Crypt Key: " + DatatypeConverter.printHexBinary(cipher1.doFinal(128, cryptAesKey)));
		System.out.println("Crypt IV: " + DatatypeConverter.printHexBinary(cipher1.doFinal(128, cryptAESiv)));

		//
		// byte[] aeskey = cd.getSecretkey();

	}

	private static byte[] createpadding(int k, byte[] data) throws Exception, NoSuchAlgorithmException {
		byte[] salt = new byte[k];
		// int ind = 0;
		// salt[ind++] = 0;
		// salt[ind++] = 2;
		// SecureRandom sc = new SecureRandom();
		// for (int i = 0, j = k - data.length - 3; i < j; i++) {
		// salt[ind++] = (byte) (sc.nextInt(254) + 1);
		// }
		// salt[ind++] = 0;
		// for (int i = 0; i < data.length; i++)
		// salt[ind++] = data[i];
		for (int i = k - data.length, j = 0; i < salt.length; i++, j++)
			salt[i] = data[j];
		return salt;
	}

	private static byte[] removePadding(int lenInBytes, byte[] dataWithPadding) throws Exception {
		if(dataWithPadding.length>lenInBytes)
			return Arrays.copyOfRange(dataWithPadding, dataWithPadding.length - lenInBytes, dataWithPadding.length);
		else
			return dataWithPadding;
	}

}
