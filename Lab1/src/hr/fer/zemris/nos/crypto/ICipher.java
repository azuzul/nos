package hr.fer.zemris.nos.crypto;

public interface ICipher {

	byte[] doFinal(int len, byte[] aESkey) throws Exception;

	int getModulusSize();

}
