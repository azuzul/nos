package hr.fer.zemris.nos.gui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.JOptionPane;

import hr.fer.zemris.nos.crypto.Crypto;
import hr.fer.zemris.nos.crypto.ICipher;
import hr.fer.zemris.nos.crypto.MyCipher;
import hr.fer.zemris.nos.data.Cryptodata;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class BasicGUI extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	private static final RadioButton method = new RadioButton("Mine rsa");

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("BasicGUI");

		Tab aes = new Tab("AES", aes());
		Tab rsa = new Tab("RSA", rsa());
		Tab sha = new Tab("SHA", sha());
		Tab choose = new Tab("Method", chooser());
		Tab digitalnaOmotnica = new Tab("Digitalna omotnica", digOmot());
		Tab digitalnaPotpis = new Tab("Digitalni potpis", digPot());
		Tab digitalnaPecat = new Tab("Digitalna pečat", digPec());
		TabPane tab = new TabPane(aes, rsa, sha, digitalnaOmotnica, digitalnaPotpis, digitalnaPecat, choose);
		tab.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

		primaryStage.setScene(new Scene(tab));
		primaryStage.show();
	}

	private Node chooser() {
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(25, 25, 25, 25));

		RadioButton rsaJava = new RadioButton("Java rsa");
		ToggleGroup tg = new ToggleGroup();
		rsaJava.setToggleGroup(tg);
		method.setToggleGroup(tg);
		rsaJava.setSelected(true);
		grid.add(rsaJava, 3, 4);
		grid.add(method, 3, 5);

		return grid;
	}

	private static Node aes() {
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(25, 25, 25, 25));

		final StringBuilder sb = new StringBuilder("aes_key.txt");
		final StringBuilder sbInput = new StringBuilder("ulaz.txt");
		final StringBuilder sbOut = new StringBuilder("aes_izlaz.txt");
		int col = 0, row = 0, colspan = 1, rowspan = 1;
		makeTitle("AES", grid, col, row++, colspan + 1, rowspan);
		Button generate = makeRowAnd3btn("Ključ:", "Odaberi", "Pregledaj", "Generiraj", sb, grid, col, row++, colspan,
				rowspan);
		makeRowAnd2btn("Ulazna datoteka:", "Odaberi", "Pregledaj", sbInput, grid, col, row++, colspan, rowspan, 2, 2);
		makeRowAnd2btn("Izlazna datoteka:", "Odaberi", "Pregledaj", sbOut, grid, col, row++, colspan, rowspan, 2, 2);
		final RadioButton crypt = makeRadio2Btn("Kriptiranje", "Dekriptiranje", grid, col, row++, colspan, rowspan);
		Button crydec = makeCentralBtn("Obavi kriptiranje/dekriptiranje", grid, col, row++, colspan, rowspan);
		makeStudentName("Ante Žužul", grid, col, row, 0, rowspan, 5);

		crydec.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					Cryptodata cd = Cryptodata.parseDocument(sb.toString(), true);
					byte[] key = cd.getSecretkey();
					byte[] iv = cd.getInitVector();

					Cipher cipher = Crypto.aes(crypt.isSelected(), key, iv);
					cd = new Cryptodata();
					if (crypt.isSelected()) {
						cd.setDescritpion("AES Encrypted");
						cd.setFileName(sbOut.toString());
						cd.setMethod(new String[] { "AES" });
						cd.encryptData(sbInput.toString(), sbOut.toString(), cipher, new String[] { "Data:" });
					} else {
						cd.decryptData(sbInput.toString(), sbOut.toString(), cipher, "Data:");
					}
				} catch (Exception e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Pogreška kod kriptiranja.", "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		generate.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				int length = 0;
				do {
					try {
						String s = JOptionPane.showInputDialog(null,
								"Upišite željenju duljinu ključa. ( Dopuštene vrijednosti su 128, 192 ili 256 bita)");
						if (s == null)
							return;
						length = Integer.parseInt(s);
					} catch (NumberFormatException ex) {
					} ;
				} while (length != 128 && length != 192 && length != 256);
				try {
					byte[] keys = Crypto.generateSymmetricKey("AES", length);
					Cryptodata cd = new Cryptodata();
					cd.setDescritpion("Secret key");
					cd.setMethod(new String[] { "AES" });
					cd.setKeylength(new int[] { length });
					cd.setFileName(sb.toString());
					cd.setSecretkey(keys);
					cd.setInitVector(Crypto.genetareIV());
					cd.saveFile(sb.toString());
				} catch (Exception e) {
					JOptionPane.showConfirmDialog(null, "Pogreška pri generiranju ključa.", "Error",
							JOptionPane.CANCEL_OPTION);
				}
			}

		});
		return grid;
	}

	private static Button makeCentralBtn(String label, GridPane grid, int col, int row, int colspan, int rowspan) {
		Button makecd = new Button(label);
		grid.add(makecd, col, row, colspan, rowspan);
		GridPane.setConstraints(makecd, col, row, colspan + 5, rowspan, HPos.CENTER, VPos.CENTER);
		return makecd;
	}

	private static void makeStudentName(String name, GridPane grid, int col, int row, int colspan, int rowspan,
			int offset) {
		Text studentName = new Text(name);
		studentName.setFont(Font.font("Arial", FontWeight.NORMAL, 13));
		grid.add(studentName, col + 5, row, colspan + 1, rowspan);
		GridPane.setConstraints(studentName, col + offset, row++, colspan + 1, rowspan, HPos.CENTER, VPos.CENTER);

	}

	private static void makeTitle(String title, GridPane pane, int col, int row, int colspan, int rowspan) {
		Text scenetitle = new Text(title);
		scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
		pane.add(scenetitle, col, row, colspan, rowspan);
	}

	private static Button makeRowAnd3btn(String label, String firstBtn, String secBtn, String thrBtn,
			final StringBuilder sb, GridPane grid, int col, int row, int colspan, int rowspan) {
		Label key = new Label(label);
		TextField keyPath = new TextField(sb.toString());
		keyPath.textProperty().addListener((observale, odlValue, newValue) -> {
			sb.setLength(0);
			sb.append(newValue);
		});

		Button keyChooseBtn = new Button(firstBtn);
		Button keyLookBtn = new Button(secBtn);
		Button keyGenerateBtn = new Button(thrBtn);
		keyChooseBtn.setMaxWidth(Double.MAX_VALUE);
		grid.add(key, col++, row);
		grid.add(keyPath, col++, row, colspan + 1, rowspan);
		col++;
		grid.add(keyChooseBtn, col++, row);
		grid.add(keyLookBtn, col++, row);
		grid.add(keyGenerateBtn, col, row);
		setChooserAction(key, keyPath, keyChooseBtn);
		setViewerAction(keyPath, keyLookBtn);
		return keyGenerateBtn;
	}

	private static void makeRowAnd2btn(String label, String firstBtn, String secBtn, StringBuilder sb, GridPane grid,
			int col, int row, int colspan, int rowspan, int titlespan, int filespan) {
		Label file = new Label(label);
		TextField filePath = new TextField(sb.toString());
		filePath.textProperty().addListener((observer, old, newT) -> {
			sb.setLength(0);
			sb.append(newT);
		});
		Button fileChooseBtn = new Button(firstBtn);
		Button fileLookBtn = new Button(secBtn);
		fileChooseBtn.setMaxWidth(Double.MAX_VALUE);
		grid.add(file, col, row, titlespan, rowspan);
		col += titlespan;
		grid.add(filePath, col, row, filespan, rowspan);
		col += filespan;
		grid.add(fileChooseBtn, col++, row);
		grid.add(fileLookBtn, col++, row);
		setChooserAction(file, filePath, fileChooseBtn);
		setViewerAction(filePath, fileLookBtn);
	}

	private static RadioButton makeRadio2Btn(String firstOption, String secondOption, GridPane grid, int col, int row,
			int colspan, int rowspan) {
		RadioButton crypt = new RadioButton(firstOption);
		RadioButton decrypt = new RadioButton(secondOption);
		final ToggleGroup cdchooser = new ToggleGroup();
		crypt.setToggleGroup(cdchooser);
		decrypt.setToggleGroup(cdchooser);
		crypt.setSelected(true);
		grid.add(crypt, 2, row);
		grid.add(decrypt, 3, row++);
		return crypt;
	}

	private static Node rsa() {
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(25, 25, 25, 25));

		int col = 0, row = 0, colspan = 1, rowspan = 1;

		StringBuilder sb1 = new StringBuilder("rsa_b_public_key.txt");
		StringBuilder sb2 = new StringBuilder("rsa_b_private_key.txt");
		StringBuilder sbInput = new StringBuilder("ulaz.txt");
		StringBuilder sbOut = new StringBuilder("rsa_izlaz.txt");

		makeTitle("RSA", grid, col, row++, colspan + 1, rowspan);
		Button generate = makeRowAnd3btn("1. Ključ:", "Odaberi", "Pregledaj", "Generiraj", sb1, grid, col, row++,
				colspan, rowspan);
		Button generate1 = makeRowAnd3btn("2. Ključ:", "Odaberi", "Pregledaj", "Generiraj", sb2, grid, col, row++,
				colspan, rowspan);
		makeRowAnd2btn("Ulazna datoteka:", "Odaberi", "Pregledaj", sbInput, grid, col, row++, colspan, rowspan, 2, 2);
		makeRowAnd2btn("Izlazna datoteka:", "Odaberi", "Pregledaj", sbOut, grid, col, row++, colspan, rowspan, 2, 2);
		final RadioButton crypt = makeRadio2Btn("Kriptiranje", "Dekriptiranje", grid, col, row++, colspan, rowspan);
		Button crydec = makeCentralBtn("Obavi kriptiranje/dekriptiranje", grid, col, row++, colspan, rowspan);
		makeStudentName("Ante Žužul", grid, col, row, 0, rowspan, 5);

		crydec.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					Cryptodata cd = Cryptodata.parseDocument((crypt.isSelected() ? sb1 : sb2).toString(), true);
					ICipher cipher = MyCipher.generateRsa(crypt.isSelected(),
							crypt.isSelected() ? cd.getPubExp() : cd.getPrivExp(), cd.getModulus(),
							method.isSelected());

					cd = new Cryptodata();
					if (crypt.isSelected()) {
						cd.encryptDataRSA(sbInput.toString(), sbOut.toString(), cipher, new String[] { "Data:" });
					} else {
						cd.decryptDataRSA(sbInput.toString(), sbOut.toString(), cipher,"Data:");
					}
				} catch (Exception e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Pogreška kod kriptiranja.", "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		EventHandler<ActionEvent> ev = new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				int length = 0;
				do {
					try {
						String s = JOptionPane.showInputDialog(null,
								"Upišite željenju duljinu ključa. ( Dopuštene vrijednosti su 1024 i 2048  bita)");
						if (s == null)
							return;
						length = Integer.parseInt(s);
					} catch (NumberFormatException ex) {
					} ;
				} while (length != 1024 && length != 2048);
				try {
					byte[][] keys = null;
					if (method.isSelected())
						keys = Crypto.generateMineAsymmetricKey(length);
					else
						keys = Crypto.generateAsymmetricKey("RSA", length);
					Cryptodata cd = new Cryptodata();
					cd.setDescritpion("Public key");
					cd.setMethod(new String[] { "RSA" });
					cd.setFileName(sb1.toString());
					cd.setKeylength(new int[] { length });
					cd.setPubExp(keys[1]); // public exp
					cd.setModulus(keys[2]); // public mod
					cd.saveFile(sb1.toString());
					cd = new Cryptodata();
					cd.setDescritpion("Private key");
					cd.setMethod(new String[] { "RSA" });
					cd.setKeylength(new int[] { length });
					cd.setFileName(sb2.toString());
					cd.setPrivExp(keys[0]);
					cd.setModulus(keys[2]); // private mod
					cd.saveFile(sb2.toString());
				} catch (Exception e) {
					e.printStackTrace();
					JOptionPane.showConfirmDialog(null, "Pogreška pri generiranju ključa.", "Error",
							JOptionPane.CANCEL_OPTION);
				}
			}

		};
		generate.setOnAction(ev);
		generate1.setOnAction(ev);
		return grid;
	}

	private static Node sha() {
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(25, 25, 25, 25));

		StringBuilder sbInput = new StringBuilder("ulaz.txt");
		StringBuilder sbOut = new StringBuilder("izlaz.txt");

		final TextArea area = new TextArea();
		area.setPrefRowCount(9);
		area.setPrefColumnCount(100);
		area.setWrapText(true);
		area.setPrefWidth(150);
		int col = 0, row = 0, colspan = 1, rowspan = 1;
		makeTitle("SHA-1", grid, col, row++, colspan + 1, rowspan);
		makeRowAnd2btn("Ulazna datoteka:", "Odaberi", "Pregledaj", sbInput, grid, col, row++, colspan, rowspan, 1, 3);
		makeRowAnd2btn("Izlazna datoteka:", "Odaberi", "Pregledaj", sbOut, grid, col, row++, colspan, rowspan, 1, 3);
		grid.add(area, col, row, 6, 9);
		row += 9;
		Button btn = makeCentralBtn("Napravi sha hash", grid, col, row++, colspan, rowspan);
		makeStudentName("Ante Žužul", grid, col, row, 0, rowspan, 5);

		btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					Cryptodata cd = new Cryptodata();
					cd.setDescritpion("SHA checksum");
					cd.setFileName(sbOut.toString());
					byte[] sig = Crypto.sha(sbInput.toString());
					cd.setSignature(sig);
					cd.setMethod(new String[] { "SHA-1" });
					cd.setKeylength(new int[] { 160 });
					cd.saveFile(sbOut.toString());
					area.setText(new BigInteger(cd.getSignature()).toString(16));
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Pogreška kod sha algoritma.", "ERROR",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		return grid;
	}

	private static Node digOmot() {
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(25, 25, 25, 25));

		StringBuilder sbInput = new StringBuilder("ulaz.txt");
		StringBuilder sbDigEnv = new StringBuilder("omotnica.txt");
		StringBuilder sbPubRec = new StringBuilder("rsa_b_public_key.txt");
		StringBuilder sbPrivRec = new StringBuilder("rsa_b_private_key.txt");
		StringBuilder sbOut = new StringBuilder("izlaz.txt");

		int col = 0, row = 0, colspan = 1, rowspan = 1;
		makeTitle("Digitalna omotnica", grid, col, row++, colspan + 1, rowspan);
		makeRowAnd2btn("Ulazna datoteka:", "Odaberi", "Pregledaj", sbInput, grid, col, row++, colspan, rowspan, 1, 3);
		makeRowAnd2btn("Javni ključ primatelja:", "Odaberi", "Pregledaj", sbPubRec, grid, col, row++, colspan, rowspan,
				1, 3);
		makeRowAnd2btn("Digitalna omotnica:", "Odaberi", "Pregledaj", sbDigEnv, grid, col, row++, colspan, rowspan, 1,
				3);
		Button genEnv = makeCentralBtn("Generiraj digitalnu omotnicu", grid, col, row++, colspan, rowspan);
		makeRowAnd2btn("Tajni ključ primatelja:", "Odaberi", "Pregledaj", sbPrivRec, grid, col, row++, colspan, rowspan,
				1, 3);
		makeRowAnd2btn("Izlazna datoteka:", "Odaberi", "Pregledaj", sbOut, grid, col, row++, colspan, rowspan, 1, 3);
		Button openEnv = makeCentralBtn("Otvori digitalnu omotnicu", grid, col, row++, colspan, rowspan);
		makeStudentName("Ante Žužul", grid, col, row, 0, rowspan, 5);

		genEnv.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					genEnv(sbInput.toString(), sbDigEnv.toString(), sbPubRec.toString());
				} catch (Exception e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Pogreška kod kriptiranja.", "Error",
							JOptionPane.INFORMATION_MESSAGE);
				}
			}

		});

		openEnv.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					openEnv(sbDigEnv.toString(), sbPrivRec.toString(), sbOut.toString());
				} catch (Exception e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Pogreška kod kriptiranja.", "Error",
							JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});

		return grid;
	}

	private static void openEnv(String digEnv, String privRec, String out) throws Exception {
		Cryptodata cd = Cryptodata.parseDocument(privRec, false);
		ICipher cipher = MyCipher.generateRsa(false, cd.getPrivExp(), cd.getModulus(), method.isSelected());
		cd = Cryptodata.parseDocument(digEnv, false);
		Cipher cipher2 = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE");
		byte[] aeskey = cd.getEnvelopeCryptKey();
		int[] keylen = cd.getKeylength();
		if (keylen == null || keylen.length != cd.getMethod().length)
			throw new Exception();
		SecretKeySpec seckey = new SecretKeySpec(cipher.doFinal(keylen[1], aeskey), "AES");
		cipher2.init(Cipher.DECRYPT_MODE, seckey, new IvParameterSpec(cipher.doFinal(keylen[1], cd.getInitVector())));
		cd.decryptData(digEnv, out, cipher2, "Envelope data:");
	}

	private static void genEnv(String input, String digEnv, String pubRec) throws Exception {
		Cryptodata cd = Cryptodata.parseDocument(pubRec, false);
		int rsalen = cd.getKeylength()[0];
		ICipher cipher = MyCipher.generateRsa(true, cd.getPubExp(), cd.getModulus(), method.isSelected());
		cd = new Cryptodata();
		cd.setDescritpion("RSA Encrypted");
		cd.setFileName(digEnv);
		cd.setMethod(new String[] { "RSA" });
		int aeslen = 128;
		byte[] AESkey = Crypto.generateSymmetricKey("AES", aeslen);
		byte[] AESiv = Crypto.genetareIV();
		cd.setEnvelopeCryptKey(cipher.doFinal(aeslen, AESkey));
		cd.setInitVector(cipher.doFinal(aeslen, AESiv));
		cd.setMethod(new String[] { "RSA", "AES" });
		cd.setKeylength(new int[] { rsalen, aeslen });
		Cipher cipher2 = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE");
		SecretKeySpec seckey = new SecretKeySpec(AESkey, "AES");
		cipher2.init(Cipher.ENCRYPT_MODE, seckey, new IvParameterSpec(AESiv));
		cd.encryptData(input, digEnv, cipher2, new String[] { "Envelope data:" });
	}

	private static Node digPot() {
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(25, 25, 25, 25));

		StringBuilder sbInput = new StringBuilder("ulaz.txt");
		StringBuilder sbPrivSend = new StringBuilder("rsa_a_private_key.txt");
		StringBuilder sbDigSig = new StringBuilder("potpis.txt");
		StringBuilder sbPubSend = new StringBuilder("rsa_a_public_key.txt");

		int col = 0, row = 0, colspan = 1, rowspan = 1;
		makeTitle("Digitalni potpis", grid, col, row++, colspan, rowspan);
		makeRowAnd2btn("Ulazna datoteka:", "Odaberi", "Pregledaj", sbInput, grid, col, row++, colspan, rowspan, 1, 3);
		makeRowAnd2btn("Tajni ključ pošiljatelja:", "Odaberi", "Pregledaj", sbPrivSend, grid, col, row++, colspan,
				rowspan, 1, 3);
		makeRowAnd2btn("Digitalni potpis:", "Odaberi", "Pregledaj", sbDigSig, grid, col, row++, colspan, rowspan, 1, 3);
		Button genSig = makeCentralBtn("Generiraj digitalni potpis", grid, col, row++, colspan, rowspan);
		makeRowAnd2btn("Javni ključ pošiljatelja:", "Odaberi", "Pregledaj", sbPubSend, grid, col, row++, colspan,
				rowspan, 1, 3);
		Button checkSig = makeCentralBtn("Provjeri digitalni potpis", grid, col, row++, colspan, rowspan);
		makeStudentName("Ante Žužul", grid, col, row, 0, rowspan, 5);

		genSig.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					genDigSign(sbInput.toString(), sbPrivSend.toString(), sbDigSig.toString());
				} catch (Exception e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Problem pri stvaranju potpisa.", "ERROR",
							JOptionPane.INFORMATION_MESSAGE);
				}
			}

		});

		checkSig.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					if (checkSign(sbInput.toString(), sbDigSig.toString(), sbPubSend.toString()))
						JOptionPane.showMessageDialog(null, "Potpis je točan.", "Provjera potpisa",
								JOptionPane.INFORMATION_MESSAGE);
					else
						JOptionPane.showMessageDialog(null, "Potpis je pogrešan.", "Provjera potpisa",
								JOptionPane.INFORMATION_MESSAGE);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		return grid;
	}

	private static boolean checkSign(String input, String digSig, String pubSend) throws Exception {
		Cryptodata cd = Cryptodata.parseDocument(digSig, false);
		Cryptodata pubCd = Cryptodata.parseDocument(pubSend, false);
		byte[] checkSum = Crypto.sha(input);
		ICipher cipher = MyCipher.generateRsa(false, pubCd.getPubExp(), true, pubCd.getModulus(), method.isSelected());
		int[] keylen = cd.getKeylength();
		if (keylen == null || keylen.length != cd.getMethod().length)
			throw new Exception();
		byte[] cryptedSum = cipher.doFinal(keylen[0], cd.getSignature());
		return Arrays.equals(checkSum, cryptedSum);
	}

	private static void genDigSign(String input, String privSend, String digSig) throws FileNotFoundException,
			UnsupportedEncodingException, Exception, IllegalBlockSizeException, BadPaddingException, IOException {
		Cryptodata cd = Cryptodata.parseDocument(privSend, false);
		ICipher cipher = MyCipher.generateRsa(true, cd.getPrivExp(), false, cd.getModulus(), method.isSelected());
		int rsalen = cd.getKeylength()[0];
		int shalen = 160;
		cd = new Cryptodata();
		cd.setDescritpion("Digital signature");
		cd.setMethod(new String[] { "SHA-1", "RSA" });
		cd.setKeylength(new int[] { shalen, rsalen });

		byte[] checkSum = Crypto.sha(input);
		cd.setSignature(cipher.doFinal(shalen, checkSum));
		cd.saveUnsecureData(digSig, input);
	}

	private static Node digPec() {
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(25, 25, 25, 25));
		int col = 0, row = 0, colspan = 1, rowspan = 1;

		StringBuilder sbInput = new StringBuilder("ulaz.txt");
		StringBuilder sbPubRec = new StringBuilder("rsa_b_public_key.txt");
		StringBuilder sbPrivSend = new StringBuilder("rsa_a_private_key.txt");
		StringBuilder sbDigEnv = new StringBuilder("omotnica.txt");
		StringBuilder sbDigSig = new StringBuilder("potpis.txt");
		StringBuilder sbPubSend = new StringBuilder("rsa_a_public_key.txt");
		StringBuilder sbPrivRec = new StringBuilder("rsa_b_private_key.txt");
		StringBuilder sbOut = new StringBuilder("izlaz.txt");

		makeTitle("Digitalni pečat", grid, col, row++, colspan, rowspan);
		makeRowAnd2btn("Ulazna datoteka:", "Odaberi", "Pregledaj", sbInput, grid, col, row++, colspan, rowspan, 1, 3);
		makeRowAnd2btn("Javni ključ primatelja:", "Odaberi", "Pregledaj", sbPubRec, grid, col, row++, colspan, rowspan,
				1, 3);
		makeRowAnd2btn("Tajni ključ pošiljatelja:", "Odaberi", "Pregledaj", sbPrivSend, grid, col, row++, colspan,
				rowspan, 1, 3);
		makeRowAnd2btn("Digitalna omotnica:", "Odaberi", "Pregledaj", sbDigEnv, grid, col, row++, colspan, rowspan, 1,
				3);
		makeRowAnd2btn("Digitalni potpis:", "Odaberi", "Pregledaj", sbDigSig, grid, col, row++, colspan, rowspan, 1, 3);
		Button genPec = makeCentralBtn("Generiraj digitalni pečat", grid, col, row++, colspan, rowspan);
		makeRowAnd2btn("Javni ključ pošiljatelja:", "Odaberi", "Pregledaj", sbPubSend, grid, col, row++, colspan,
				rowspan, 1, 3);
		makeRowAnd2btn("Tajni ključ primatelja:", "Odaberi", "Pregledaj", sbPrivRec, grid, col, row++, colspan, rowspan,
				1, 3);
		makeRowAnd2btn("Izlazna datoteka:", "Odaberi", "Pregledaj", sbOut, grid, col, row++, colspan, rowspan, 1, 3);
		Button openPec = makeCentralBtn("Otvori digitalni pečat", grid, col, row++, colspan, rowspan);
		makeStudentName("Ante Žužul", grid, col, row, 0, rowspan, 5);

		genPec.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					genEnv(sbInput.toString(), sbDigEnv.toString(), sbPubRec.toString());
					genDigSign(sbDigEnv.toString(), sbPrivSend.toString(), sbDigSig.toString());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		openPec.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					if (checkSign(sbDigEnv.toString(), sbDigSig.toString(), sbPubSend.toString())) {
						openEnv(sbDigEnv.toString(), sbPrivRec.toString(), sbOut.toString());
					} else
						JOptionPane.showMessageDialog(null, "Potpis je pogrešan.", "Provjera potpisa",
								JOptionPane.INFORMATION_MESSAGE);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		return grid;
	}

	private static void setChooserAction(Label name, final TextField field, Button btn) {
		btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				FileChooser fileChooser = new FileChooser();
				fileChooser.setTitle(name.getText().substring(0, name.getText().length() - 1));
				if (!field.getText().isEmpty())
					fileChooser.setInitialFileName(field.getText());
				File file = fileChooser.showOpenDialog(null);
				if (file != null)
					field.setText(file.getAbsolutePath());
			}
		});
	}

	private static void setViewerAction(final TextField field, Button btn) {
		btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					File f = new File(field.getText());
					if (f.exists() && f.isFile()) {
						Runtime rs = Runtime.getRuntime();
						try {
							rs.exec("gedit " + f.getAbsolutePath());
						} catch (Exception e) {
							System.out.println("AAAA");
						}
					}
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Unesena datoteka ne postoji", "File error",
							JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
	}
}
