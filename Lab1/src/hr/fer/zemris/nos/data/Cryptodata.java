package hr.fer.zemris.nos.data;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Base64;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.ShortBufferException;
import javax.xml.bind.DatatypeConverter;

import hr.fer.zemris.nos.crypto.ICipher;

public class Cryptodata {

	private static final int stringLength = 60;
	private String descritpion;
	private String fileName;
	private String[] method;
	private int[] keylength;
	private byte[] secretkey;
	private byte[] initVector;
	private byte[] modulus;
	private byte[] pubExp;
	private byte[] privExp;
	private byte[] signature;
	private String data;
	private String envelopeData;
	private byte[] envelopeCryptKey;

	private static final String start = "---BEGIN OS2 CRYPTO DATA---";
	private static final String end = "---END OS2 CRYPTO DATA---";
	private static final String padding = "    ";

	public Cryptodata() {
	}

	public static Cryptodata parseDocument(String fileName, boolean readData)
			throws FileNotFoundException, UnsupportedEncodingException {
		Scanner sc = new Scanner(new File(fileName));
		while (!sc.nextLine().trim().equals(start));
		String line = null;
		while ((line = sc.nextLine()).isEmpty());
		Cryptodata cd = new Cryptodata();
		while (sc.hasNextLine() && !line.equals(end)) {
			if (line.equals("Description:")) {
				testString((line = sc.nextLine()), sc);
				cd.descritpion = line.trim();
				while (!line.equals(end) && (line = sc.nextLine()).isEmpty());
			} else if (line.equals("File name:")) {
				testString((line = sc.nextLine()), sc);
				cd.fileName = line.trim();
				while (!line.equals(end) && (line = sc.nextLine()).isEmpty());
			} else if (line.equals("Method:")) {
				List<String> tmp = new LinkedList<>();
				while (!(line = sc.nextLine()).isEmpty() && !line.equals(end)) {
					testString(line, sc);
					tmp.add(line);
				}
				if (tmp.isEmpty())
					throw new IllegalArgumentException("Metoda nije upisana.");
				cd.method = tmp.stream().map(String::trim).toArray(String[]::new);
				while (!line.equals(end) && (line = sc.nextLine()).isEmpty());
			} else if (line.equals("Key length:")) {
				cd.keylength = new int[cd.method.length];
				int i = 0;
				while (!(line = sc.nextLine()).isEmpty() && !line.equals(end)) {
					testString(line, sc);
					try {
						cd.keylength[i++] = Integer.parseInt(line.trim(), 16);
					} catch (IndexOutOfBoundsException ex) {
						throw new IllegalArgumentException("Uneseno je previse duljina kljuceva.");
					}
				}
				while (!line.equals(end) && (line = sc.nextLine()).isEmpty());
			} else if (line.equals("Secret key:")) {
				cd.secretkey = DatatypeConverter.parseHexBinary(readChunk(sc));
				while (!line.equals(end) && (line = sc.nextLine()).isEmpty());
			} else if (line.equals("Initialization vector:")) {
				cd.initVector = DatatypeConverter.parseHexBinary(readChunk(sc));
				while (!line.equals(end) && (line = sc.nextLine()).isEmpty());
			} else if (line.equals("Modulus:")) {
				cd.modulus = DatatypeConverter.parseHexBinary(readChunk(sc));
				while (!line.equals(end) && (line = sc.nextLine()).isEmpty());
			} else if (line.equals("Public exponent:")) {
				cd.pubExp = DatatypeConverter.parseHexBinary(readChunk(sc));
				while (!line.equals(end) && (line = sc.nextLine()).isEmpty());
			} else if (line.equals("Private exponent:")) {
				cd.privExp = DatatypeConverter.parseHexBinary(readChunk(sc));
				while (!line.equals(end) && (line = sc.nextLine()).isEmpty());
			} else if (line.equals("Signature:")) {
				cd.signature = DatatypeConverter.parseHexBinary(readChunk(sc));
				while (!line.equals(end) && (line = sc.nextLine()).isEmpty());
			} else if (line.equals("Data:")) {
				if (readData)
					cd.data = readChunk(sc);
				else
					while (!(line = sc.nextLine()).isEmpty() && !line.equals(end));
				while (!line.equals(end) && (line = sc.nextLine()).isEmpty());
			} else if (line.equals("Envelope data:")) {
				if (readData)
					cd.envelopeData = readChunk(sc);
				else
					while (!(line = sc.nextLine()).isEmpty() && !line.equals(end));
				while (!line.equals(end) && (line = sc.nextLine()).isEmpty());
			} else if (line.equals("Envelope crypt key:")) {
				cd.envelopeCryptKey = DatatypeConverter.parseHexBinary(readChunk(sc));
				while (!line.equals(end) && (line = sc.nextLine()).isEmpty());
			}
		}
		sc.close();
		return cd;
	}

	private static String readChunk(Scanner sc) {
		String line;
		StringBuffer buf = new StringBuffer();
		while (!(line = sc.nextLine()).isEmpty()) {
			testString(line, sc);
			buf.append(line.trim());
		}
		return buf.toString();
	}

	public void saveFile(String fileName) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(new File(fileName)));
		writeHeader(bw);
		writeChunks(bw, "Data:", data);
		writeChunks(bw, "Envelope data:", envelopeData);
		writeChunks(bw, "Envelope crypt key:", hexaToString(envelopeCryptKey));
		bw.write(end);
		bw.close();
	}

	public void saveUnsecureData(String outputFile, String inputFile) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(new File(outputFile)));

		writeHeader(bw);

		bw.write("Data:");
		bw.newLine();
		bw.flush();

		Scanner sc = new Scanner(new File(inputFile));
		StringBuilder sb = new StringBuilder();
		StringBuilder sb2 = new StringBuilder();
		while (sc.hasNextLine()) {
			sb2.append(sc.nextLine());
			if (sb2.length() > 120) {
				sb.append(new String(Base64.getEncoder().encode(sb2.substring(0, 120).getBytes("UTF-8")), "UTF-8"));
				sb2.delete(0, 120);
				writeLongRow(bw, sb);
			}
		}

		writeLongRow(bw, sb);
		if (sb.length() > 0) {
			bw.write(padding + sb.toString());
			bw.newLine();
			bw.flush();
		}

		sc.close();

		writeChunks(bw, "Envelope data:", envelopeData);
		writeChunks(bw, "Envelope crypt key:", hexaToString(envelopeCryptKey));
		bw.write(end);
		bw.close();
	}

	public void loadUnsecureData(String outputFile, String inputFile) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(new File(outputFile)));
		Scanner sc = new Scanner(new File(inputFile));

		while (!sc.nextLine().equals("Data:"));
		String line = "";
		while (!(line = sc.nextLine().trim()).isEmpty()) {
			bw.write(new String(Base64.getDecoder().decode(line.getBytes("UTF-8")), "UTF-8"));
			bw.newLine();
			bw.flush();
		}

		sc.close();
		bw.close();
	}

	private void writeHeader(BufferedWriter bw) throws IOException {
		bw.write(start);
		bw.newLine();

		writeChunks(bw, "Description:", descritpion);
		writeChunks(bw, "File name:", this.fileName);
		if (method != null) {
			bw.write("Method:");
			bw.newLine();
			for (String s : method) {
				bw.write(padding + s);
				bw.newLine();
			}
			bw.newLine();
			bw.flush();
		}
		if (keylength != null) {
			bw.write("Key length:");
			bw.newLine();
			for (int i : keylength) {
				String str = Integer.toHexString(i);
				bw.write(padding + (Character.isAlphabetic(str.charAt(0)) ? str : "0" + str));
				bw.newLine();
			}
			bw.newLine();
			bw.flush();
		}
		writeChunks(bw, "Secret key:", hexaToString(secretkey));
		writeChunks(bw, "Initialization vector:", hexaToString(initVector));
		writeChunks(bw, "Modulus:", hexaToString(modulus));
		writeChunks(bw, "Public exponent:", hexaToString(pubExp));
		writeChunks(bw, "Private exponent:", hexaToString(privExp));
		writeChunks(bw, "Signature:", hexaToString(signature));
	}

	public void encryptData(String inputFile, String outFile, Cipher cipher, String[] data) throws Exception {
		BufferedWriter bw = new BufferedWriter(new FileWriter(new File(outFile)));
		writeHeader(bw);
		for (String s : data) {
			bw.write(s);
			bw.newLine();

			writeData(inputFile, cipher, bw);
		}

		writeChunks(bw, "Envelope crypt key:", hexaToString(envelopeCryptKey));
		bw.write(end);
		bw.close();
	}

	private void writeData(String inputFile, Cipher cipher, BufferedWriter bw)
			throws FileNotFoundException, IOException, ShortBufferException, UnsupportedEncodingException,
			IllegalBlockSizeException, BadPaddingException {
		BufferedInputStream bis = new BufferedInputStream(new FileInputStream(new File(inputFile)));
		int size = 4096;
		byte[] inputBuff = new byte[size];
		int read = 0;
		StringBuilder sb = new StringBuilder();
		int ostatak = 0;
		byte[] visak = new byte[size * 2];
		while ((read = bis.read(inputBuff, ostatak, size - ostatak)) >= 1) {
			byte[] outputBuff = new byte[size];
			int updated = cipher.update(inputBuff, 0, read, outputBuff);
			int upd = (int) ((ostatak + updated) / 3.) * 3;
			byte[] arrToEnc = new byte[upd];
			for (int i = 0; i < ostatak; i++)
				arrToEnc[i] = visak[i];
			for (int i = 0;  i + ostatak < upd; i++)
				arrToEnc[ostatak + i] = outputBuff[i];
			sb.append(new String(Base64.getEncoder().encode(arrToEnc), "UTF-8"));

			writeLongRow(bw, sb);
			ostatak = updated + ostatak - upd;
			if (ostatak > 0) {
				for (int i = 0; i < ostatak; i++)
					visak[i] = outputBuff[updated - ostatak + i];
			}
		}

		byte[] outputBuff = new byte[size];
		int updated = cipher.doFinal(inputBuff, 0, (read < 0 ? 0 : read), outputBuff);
		byte[] out = new byte[ostatak + updated];
		for (int i = 0; i < ostatak; i++)
			out[i] = visak[i];
		for (int i = 0; i < updated; i++)
			out[ostatak + i] = outputBuff[i];
		sb.append(new String(Base64.getEncoder().encode(out), "UTF-8"));
		writeLongRow(bw, sb);
		if (sb.length() > 0) {
			bw.write(padding + sb.toString());
			bw.newLine();
		}

		bis.close();
		bw.newLine();
	}

	private void writeLongRow(BufferedWriter bw, StringBuilder sb) throws IOException {
		while (sb.length() > stringLength) {
			bw.write(padding + sb.substring(0, stringLength));
			sb.delete(0, stringLength);
			bw.newLine();
			bw.flush();
		}
	}

	public void decryptData(String inputFile, String outputFile, Cipher cipher, String data) throws Exception {
		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(new File(outputFile)));

		Scanner sc = new Scanner(new File(inputFile), "UTF-8");
		byte[] inputBuff = new byte[4096];
		while (!data.equals(sc.nextLine()));
		StringBuilder sb = new StringBuilder(4096);
		while (!(data = sc.nextLine().trim()).isEmpty()) {
			sb.append(data);
			if (sb.length() >= 4096) {
				inputBuff = Base64.getDecoder().decode(sb.substring(0, 4096).getBytes("UTF-8"));
				byte[] outputBuff = new byte[4096];
				int updated = cipher.update(inputBuff, 0, inputBuff.length, outputBuff);
				bos.write(outputBuff, 0, updated);
				bos.flush();
				sb.delete(0, 4096);
			}
		}

		byte[] outputBuff = new byte[4096];
		inputBuff = new byte[4096];
		int updated = 0;
		if (sb.length() > 0) {
			inputBuff = Base64.getDecoder().decode(sb.toString().getBytes("UTF-8"));
			updated = cipher.update(inputBuff, 0, inputBuff.length, outputBuff);
			bos.write(outputBuff, 0, updated);
		}

		inputBuff = Base64.getDecoder().decode(sb.toString().getBytes("UTF-8"));
		outputBuff = cipher.doFinal();
		bos.write(outputBuff);

		sc.close();
		bos.close();
	}

	public void encryptDataRSA(String inputFile, String outFile, ICipher cipher, String[] data) throws Exception {
		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(new File(outFile)));
		writeDataRSA(inputFile, cipher, bos);
		bos.close();
	}

	private void writeDataRSA(String inputFile, ICipher cipher, BufferedOutputStream bw) throws Exception {
		BufferedInputStream bis = new BufferedInputStream(new FileInputStream(new File(inputFile)));
		int size = cipher.getModulusSize()/8;
		byte[] inputBuff = new byte[size];
		int read = 0;
		while ((read = bis.read(inputBuff, 0, size)) >= 1) {
			byte[] outputBuff = cipher.doFinal(size, Arrays.copyOfRange(inputBuff, 0, read));
			bw.write(outputBuff);
		}

		bis.close();
	}

	public void decryptDataRSA(String inputFile, String outputFile, ICipher cipher, String data) throws Exception {
		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(new File(outputFile)));
		BufferedInputStream bis = new BufferedInputStream(new FileInputStream(new File(inputFile)));
		int n = cipher.getModulusSize();

		
		byte[] inputBuff = new byte[n];
		int read = 0;
		while ((read = bis.read(inputBuff, 0, n)) >= 1) {
			byte[] outputBuff = cipher.doFinal(n, Arrays.copyOfRange(inputBuff, 0, read));
			int i = 0;
			while (outputBuff[i] == 0)
				i++;
			bos.write(outputBuff, i, outputBuff.length - i);
		}
		bis.close();
		bos.close();
	}

	private String hexaToString(byte[] field) {
		if (field == null)
			return null;
		return DatatypeConverter.printHexBinary(field);// new BigInteger(1,
														// field).toString(16);
	}

	private void writeChunks(BufferedWriter bw, String fieldName, String fieldValue) throws IOException {
		if (fieldValue == null)
			return;
		bw.write(fieldName);
		bw.newLine();
		for (String s : chunks(fieldValue, stringLength)) {
			bw.write(padding + s);
			bw.newLine();
		}
		bw.newLine();
		bw.flush();
	}

	private static String[] chunks(String input, int length) {
		String[] chunks = new String[(int) Math.ceil((double) input.length() / length)];
		for (int i = 0, start = 0, end = length; i < chunks.length; i++, start = end, end = start + length) {
			chunks[i] = i + 1 == chunks.length ? input.substring(start) : input.substring(start, end);
		}
		return chunks;
	}

	private static void testString(String str, Scanner sc) {
		if (!str.startsWith(padding)) {
			sc.close();
			throw new IllegalArgumentException("Krivo upisana metoda.");
		}
	}

	public String getDescritpion() {
		return descritpion;
	}

	public void setDescritpion(String descritpion) {
		this.descritpion = descritpion;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String[] getMethod() {
		return method;
	}

	public void setMethod(String[] method) {
		this.method = method;
	}

	public int[] getKeylength() {
		return keylength;
	}

	public void setKeylength(int[] keylength) {
		this.keylength = keylength;
	}

	public byte[] getSecretkey() {
		return secretkey;
	}

	public void setSecretkey(byte[] secretkey) {
		this.secretkey = secretkey;
	}

	public byte[] getInitVector() {
		return initVector;
	}

	public void setInitVector(byte[] initVector) {
		this.initVector = initVector;
	}

	public byte[] getModulus() {
		return modulus;
	}

	public void setModulus(byte[] modulus) {
		this.modulus = modulus;
	}

	public byte[] getPubExp() {
		return pubExp;
	}

	public void setPubExp(byte[] pubExp) {
		this.pubExp = pubExp;
	}

	public byte[] getPrivExp() {
		return privExp;
	}

	public void setPrivExp(byte[] privExp) {
		this.privExp = privExp;
	}

	public byte[] getSignature() {
		return signature;
	}

	public void setSignature(byte[] signature) {
		this.signature = signature;
	}

	public String getData() {
		return new String(Base64.getDecoder().decode(data.getBytes()));
	}

	public void setData(String data) {
		this.data = new String(Base64.getEncoder().encode(data.getBytes()));
	}

	public String getEnvelopeData() {
		return new String(Base64.getDecoder().decode(envelopeData.getBytes()));
	}

	public void setEnvelopeData(String envelopeData) {
		this.envelopeData = new String(Base64.getEncoder().encode(envelopeData.getBytes()));
	}

	public byte[] getEnvelopeCryptKey() {
		return envelopeCryptKey;
	}

	public void setEnvelopeCryptKey(byte[] envelopeCryptKey) {
		this.envelopeCryptKey = envelopeCryptKey;
	}

	public static String getStart() {
		return start;
	}

	public static String getEnd() {
		return end;
	}

}
